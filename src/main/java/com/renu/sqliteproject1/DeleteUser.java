/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.renu.sqliteproject1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ray
 */
public class DeleteUser {
     public static void main(String[] args){
         Connection conn = null;
        String dbName = "user.db";
        Statement stmt = null;
        
        
         try {
             Class.forName("org.sqlite.JDBC");
             conn = DriverManager.getConnection("jdbc:sqlite:" +dbName);
             conn.setAutoCommit(false);
             stmt = conn.createStatement();
             //Update
             stmt.executeUpdate("DELETE from USER where ID=2");
             conn.commit();
             //selet
             ResultSet rs = stmt.executeQuery("SELECT*FROM USER");
             
             while(rs.next()){
                 int id = rs.getInt("id");
                 String name = rs.getString("username");
                 String password = rs.getString("password");
                 System.out.println("ID =" +id);
                 System.out.println("USERNAME=" +name);
                 System.out.println("PASSWORD =" +password);
                         
             }
             rs.close();
             stmt.close();

             conn.close();
         } catch (ClassNotFoundException ex) {
             Logger.getLogger(InsertCompany.class.getName()).log(Level.SEVERE, null, ex);
         } catch (SQLException ex) {
             Logger.getLogger(InsertCompany.class.getName()).log(Level.SEVERE, null, ex);
         }
     }




}
