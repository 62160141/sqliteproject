/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.renu.sqliteproject1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ray
 */
public class Inseruser {
    public static void main(String[] args){
         Connection conn = null;
        String dbName = "user.db";
        Statement stmt = null;
        
        
         try {
             Class.forName("org.sqlite.JDBC");
             conn = DriverManager.getConnection("jdbc:sqlite:" +dbName);
             conn.setAutoCommit(false);
             stmt = conn.createStatement();
             String sql = "INSERT INTO USER (ID,USERNAME,PASSWORD) "
                     + "VALUES (1, 'pai', 'password' );";
             stmt.executeUpdate(sql);
            

            sql = "INSERT INTO USER (ID,USERNAME,PASSWORD) "
                     + "VALUES (2, 'user1', 'password' );";
             stmt.executeUpdate(sql);

             sql = "INSERT INTO USER (ID,USERNAME,PASSWORD) "
                     + "VALUES (4, 'user2', 'password' );";
             stmt.executeUpdate(sql);

             sql = "INSERT INTO USER (ID,USERNAME,PASSWORD) "
                     + "VALUES (3, 'user3', 'password' );";
             stmt.executeUpdate(sql);
             stmt.close();
             conn.commit();

             conn.close();
         } catch (ClassNotFoundException ex) {
             Logger.getLogger(InsertCompany.class.getName()).log(Level.SEVERE, null, ex);
         } catch (SQLException ex) {
             Logger.getLogger(InsertCompany.class.getName()).log(Level.SEVERE, null, ex);
         }
     }
}

